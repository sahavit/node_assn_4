var fs = require('fs');

var resultObj = {};

var readStream1 = fs.createReadStream('./file1.txt','utf-8')
var readStream2 = fs.createReadStream('./file2.txt','utf-8')
var writeStream = fs.createWriteStream('./result.txt','utf-8')

function computeResult(data){
    let dataObj = JSON.parse(data);

    var status = dataObj.status;
    var total = dataObj.total;
    resultObj[status] = total;
}

readStream1.on('data', computeResult)
readStream2.on('data', computeResult)
readStream2.on('end', function(){
    writeStream.write(JSON.stringify(resultObj))
})
