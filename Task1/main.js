var fs = require('fs');

var readStream1 = fs.createReadStream('./file1.txt','utf-8')
var readStream2 = fs.createReadStream('./file2.txt','utf-8')
var readStream3 = fs.createReadStream('./file3.txt','utf-8')
var writeStream = fs.createWriteStream('./result.txt')

readStream1.pipe(writeStream)
readStream2.pipe(writeStream)
readStream3.pipe(writeStream)